USE nomina_rinku;
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID ('nomina_rinku.dbo.proc_utileriaempleado','P') IS NOT NULL 
	DROP PROCEDURE  proc_utileriaempleado
GO

-- =============================================
-- Author:		CHRISTIAN ENRIQUE SAPIEN GAMEZ
-- Create date: 15/NOV/2018
-- Description:	UTILERIA DE EMPLEADO
-- =============================================
CREATE PROCEDURE proc_utileriaempleado 
	@iOpcion INT, 
	@iClave_empleado INT, 
	@cNombre VARCHAR(250),
	@iClave_rol INT,
	@iClave_tipo INT
AS
BEGIN
	SET NOCOUNT ON;
	
	IF @iOpcion = 1
	BEGIN
		INSERT INTO nomina_rinku.dbo.empleado(nombre, clave_rol, clave_tipo) VALUES (
            @cNombre,
            @iClave_rol,
            @iClave_tipo);
		SELECT TOP 1 clave_empleado
			FROM nomina_rinku.dbo.empleado 
			WHERE nombre = @cNombre and clave_rol = @iClave_rol and clave_tipo = @iClave_tipo ORDER BY clave_empleado DESC;
	END	
	ELSE IF @iOpcion = 2
	BEGIN
		SELECT empleado.clave_empleado, empleado.nombre, rol.nombre AS rol, tipo.nombre AS tipo
			FROM empleado 
			INNER JOIN rol ON empleado.clave_rol = rol.clave_rol 
			INNER JOIN tipo ON empleado.clave_tipo = tipo.clave_tipo;
	END
	ELSE IF @iOpcion = 3
	BEGIN
		SELECT clave_empleado, nombre, clave_rol, clave_tipo
			FROM nomina_rinku.dbo.empleado 
			WHERE clave_empleado = @iClave_empleado;
	END
	ELSE IF @iOpcion = 4
	BEGIN
		UPDATE nomina_rinku.dbo.empleado SET 
            nombre = @cNombre,
            clave_rol = @iClave_rol,
            clave_tipo = @iClave_tipo
            WHERE clave_empleado = @iClave_empleado;
		SELECT @@ROWCOUNT AS rowAfectadas

	END
	ELSE IF @iOpcion = 5
	BEGIN
		DELETE FROM nomina_rinku.dbo.empleado 
            WHERE clave_empleado = @iClave_empleado;
		SELECT @@ROWCOUNT AS rowAfectadas
	END
END
GO
