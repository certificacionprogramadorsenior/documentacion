USE nomina_rinku;
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID ('nomina_rinku.dbo.proc_utilerianomina','P') IS NOT NULL 
	DROP PROCEDURE  proc_utilerianomina
GO

-- =============================================
-- Author:		CHRISTIAN ENRIQUE SAPIEN GAMEZ
-- Create date: 21/NOV/2018
-- Description:	UTILERIA DE NOMINA
-- =============================================
CREATE PROCEDURE proc_utilerianomina
	@iOpcion INT,
	@iMes INT,
	@iAno INT
AS
BEGIN
	SET NOCOUNT ON;
		
	IF @iOpcion = 1
	BEGIN
		DECLARE @SueldoBase FLOAT,
				@PorEntrega FLOAT,
				@BonoChofer FLOAT,
				@BonoCargadores FLOAT,
				@BonoAuxiliares FLOAT;
				
		SELECT @SueldoBase = valor FROM nomina_rinku.dbo.configuracion_nomina WHERE clave_conf = 1;
		SELECT @PorEntrega = valor FROM nomina_rinku.dbo.configuracion_nomina WHERE clave_conf = 5;
		
		SELECT @BonoChofer = bono FROM nomina_rinku.dbo.rol WHERE clave_rol = 1;
		SELECT @BonoCargadores = bono FROM nomina_rinku.dbo.rol WHERE clave_rol = 2;
		SELECT @BonoAuxiliares = bono FROM nomina_rinku.dbo.rol WHERE clave_rol = 3;
		
		--ACTUALIZA EL SUELDO BASE DIARIO Y SUELDO POR ENTREGAS 
		UPDATE nomina_rinku.dbo.movimiento SET
			sueldo_base = horas_trabajadas * @SueldoBase,
			sueldo_entrega = entrega * @PorEntrega,
			pagado = 1
			WHERE 
				(SELECT DATEPART(month, fecha))=@iMes AND
				(SELECT DATEPART(year, fecha))=@iAno;
		
		--ACTUALIZA BONO CHOFERES
		UPDATE nomina_rinku.dbo.movimiento SET
			sueldo_bono = horas_trabajadas * @BonoChofer
			WHERE 
				clave_empleado IN (SELECT clave_empleado FROM empleado WHERE empleado.clave_rol = 1) AND
				(SELECT DATEPART(month, fecha))=@iMes AND
				(SELECT DATEPART(year, fecha))=@iAno;
				
		--ACTUALZIA BONO CARGADORES
		UPDATE nomina_rinku.dbo.movimiento SET
			sueldo_bono = horas_trabajadas * @BonoCargadores
			WHERE 
				clave_empleado IN (SELECT clave_empleado FROM empleado WHERE empleado.clave_rol = 2) AND
				(SELECT DATEPART(month, fecha))=@iMes AND
				(SELECT DATEPART(year, fecha))=@iAno;
				
		--ACTUALIZA BONO AUXILIAR NO CUBRIO
		UPDATE nomina_rinku.dbo.movimiento SET
			sueldo_bono = horas_trabajadas * @BonoAuxiliares
			WHERE 
				clave_empleado IN (SELECT clave_empleado FROM empleado WHERE empleado.clave_rol = 3) AND
				clave_rol_cubrio = 0 AND
				(SELECT DATEPART(month, fecha))=@iMes AND
				(SELECT DATEPART(year, fecha))=@iAno;
				
		--ACTUALIZA BONO AUXILIAR CUBRIO CHOFER
		UPDATE nomina_rinku.dbo.movimiento SET
			sueldo_bono = horas_trabajadas * @BonoChofer
			WHERE 
				clave_empleado IN (SELECT clave_empleado FROM empleado WHERE empleado.clave_rol = 3) AND
				clave_rol_cubrio = 1 AND
				(SELECT DATEPART(month, fecha))=@iMes AND
				(SELECT DATEPART(year, fecha))=@iAno;
				
		--ACTUALIZA BONO AUXILIAR CUBRIO CARGADOR		
		UPDATE nomina_rinku.dbo.movimiento SET
			sueldo_bono = horas_trabajadas * @BonoCargadores
			WHERE 
				clave_empleado IN (SELECT clave_empleado FROM empleado WHERE empleado.clave_rol = 3) AND
				clave_rol_cubrio = 2 AND
				(SELECT DATEPART(month, fecha))=@iMes AND
				(SELECT DATEPART(year, fecha))=@iAno;
	END
	ELSE 
	IF @iOpcion = 2
	BEGIN
		DECLARE @TablaDevolver TABLE(clave_empleado INT, nombre VARCHAR(250), rol VARCHAR(250), tipo VARCHAR(250), SueldoBase FLOAT, SueldoEntrega FLOAT, SueldoBono FLOAT);
		
		INSERT INTO @TablaDevolver(clave_empleado, nombre, rol, tipo, SueldoBase, SueldoEntrega, SueldoBono) 
			SELECT movimiento.clave_empleado, MAX(empleado.nombre) AS nombre, MAX(rol.nombre) AS rol, MAX(tipo.nombre) AS tipo, SUM(movimiento.sueldo_base) AS SueldoBase, SUM(movimiento.sueldo_entrega) AS SueldoEntrega, SUM(movimiento.sueldo_bono) AS SueldoBono
			FROM movimiento 
			INNER JOIN empleado ON movimiento.clave_empleado = empleado.clave_empleado 
			INNER JOIN rol ON empleado.clave_rol = rol.clave_rol 
			INNER JOIN tipo ON empleado.clave_tipo = tipo.clave_tipo
			WHERE 
				pagado = 1 
				AND (SELECT DATEPART(month, fecha))=@iMes 
				AND (SELECT DATEPART(year, fecha))=@iAno
			GROUP BY movimiento.clave_empleado;
			
		SELECT clave_empleado, nombre, rol, tipo, SueldoBase, SueldoEntrega, SueldoBono FROM @TablaDevolver;
	END	
END
GO
