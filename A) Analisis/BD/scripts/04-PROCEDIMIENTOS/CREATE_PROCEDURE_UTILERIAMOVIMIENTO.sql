USE nomina_rinku;
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID ('nomina_rinku.dbo.proc_utileriamovimiento','P') IS NOT NULL 
	DROP PROCEDURE  proc_utileriamovimiento
GO

-- =============================================
-- Author:		CHRISTIAN ENRIQUE SAPIEN GAMEZ
-- Create date: 18/NOV/2018
-- Description:	UTILERIA DE MOVIMIENTO
-- =============================================
CREATE PROCEDURE proc_utileriamovimiento
	@iOpcion INT,
	@iClave_movimiento INT,
	@iClave_empleado INT, 
	@iClave_rol_cubrio INT,
	@dFecha VARCHAR(100),
	@iHoras_trabajadas INT,
	@iEntrega INT
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @fec DATETIME;

	IF @iOpcion = 1
	BEGIN
		SELECT @fec = CONVERT(DATETIME, @dFecha, 111);
		INSERT INTO nomina_rinku.dbo.movimiento(clave_empleado, clave_rol_cubrio, fecha, horas_trabajadas, entrega, pagado) VALUES (
			@iClave_empleado,
			@iClave_rol_cubrio,
			@fec,
			@iHoras_trabajadas,
			@iEntrega,
			0
		);
		
		SELECT @@ROWCOUNT AS rowAfectadas;
	END	
	ELSE IF @iOpcion = 2
	BEGIN
		SELECT movimiento.clave_movimiento, movimiento.clave_empleado, movimiento.fecha, empleado.nombre, rol.nombre AS rol, tipo.nombre AS tipo
			FROM movimiento 
			INNER JOIN empleado ON movimiento.clave_empleado = empleado.clave_empleado 
			INNER JOIN rol ON empleado.clave_rol = rol.clave_rol 
			INNER JOIN tipo ON empleado.clave_tipo = tipo.clave_tipo
			WHERE pagado = 0
			ORDER BY clave_movimiento DESC;
	END
	ELSE IF @iOpcion = 3
	BEGIN
		SELECT movimiento.clave_movimiento, movimiento.clave_empleado, empleado.nombre, tipo.nombre AS tipo, rol.nombre AS rol, movimiento.fecha, movimiento.clave_rol_cubrio, movimiento.horas_trabajadas, movimiento.entrega                 
			FROM empleado 
			INNER JOIN movimiento ON empleado.clave_empleado = movimiento.clave_empleado 
			INNER JOIN rol ON empleado.clave_rol = rol.clave_rol 
			INNER JOIN tipo ON empleado.clave_tipo = tipo.clave_tipo
			WHERE movimiento.clave_movimiento = @iClave_movimiento;
	END
	ELSE IF @iOpcion = 4
	BEGIN
		SELECT @fec = CONVERT(DATETIME, @dFecha, 111);
		UPDATE nomina_rinku.dbo.movimiento SET
			clave_empleado = @iClave_empleado, 
			clave_rol_cubrio = @iClave_rol_cubrio, 
			fecha = @fec,
			horas_trabajadas = @iHoras_trabajadas,
			entrega = @iEntrega
			WHERE clave_movimiento = @iClave_movimiento;
			
		SELECT @@ROWCOUNT AS rowAfectadas;
	END
	ELSE IF @iOpcion = 5
	BEGIN
		DELETE FROM nomina_rinku.dbo.movimiento
			WHERE clave_movimiento = @iClave_movimiento;

		SELECT @@ROWCOUNT AS rowAfectadas;
	END
	ELSE IF @iOpcion = 6
	BEGIN
		--SELECT CONVERT(date, GETDATE()) AS fecha;
		SELECT GETDATE() AS fecha;
	END
	ELSE IF @iOpcion = 7
	BEGIN
		SELECT empleado.clave_empleado, empleado.nombre, rol.nombre AS rol, tipo.nombre AS tipo
			FROM empleado 
			INNER JOIN rol ON empleado.clave_rol = rol.clave_rol 
			INNER JOIN tipo ON empleado.clave_tipo = tipo.clave_tipo
			WHERE empleado.clave_empleado = @iClave_empleado;
	END
END
GO
