USE nomina_rinku
GO

--IF EXISTS ( SELECT NAME FROM SysObjects(NOLOCK) WHERE NAME = 'tipo' ) 
--    DROP TABLE tipo
--GO

/*****************************************************************************
BASE DATOS;     nomina_rinku
TABLA:          tipo
FECHA:          14/11/2018
DESCRIPCI�N:    Tabla encargada de almacenar los tipo
REALIZ�:        Sapien Gamez Christian Enrique
*****************************************************************************/

CREATE TABLE tipo(
	clave_tipo	INT IDENTITY(1,1) NOT NULL PRIMARY KEY,
	nombre		VARCHAR(250) NOT NULL DEFAULT ''
)
GO