USE nomina_rinku
GO

--IF EXISTS ( SELECT NAME FROM SysObjects(NOLOCK) WHERE NAME = 'empleado' ) 
--    DROP TABLE empleado
--GO

/*****************************************************************************
BASE DATOS;     nomina_rinku
TABLA:          empleado
FECHA:          14/11/2018
DESCRIPCI�N:    Tabla encargada de almacenar los empleados
REALIZ�:        Sapien Gamez Christian Enrique
*****************************************************************************/

CREATE TABLE empleado(
	clave_empleado	INT IDENTITY(1,1) NOT NULL PRIMARY KEY,
	nombre			VARCHAR(250) NOT NULL DEFAULT '',
	clave_rol		INT NOT NULL DEFAULT -1,
	clave_tipo		INT NOT NULL DEFAULT -1
)
GO