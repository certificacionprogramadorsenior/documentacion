USE nomina_rinku
GO

--IF EXISTS ( SELECT NAME FROM SysObjects(NOLOCK) WHERE NAME = 'nomina' ) 
--    DROP TABLE movimiento

/*****************************************************************************
BASE DATOS;     nomina_rinku
TABLA:          movimiento
FECHA:          14/11/2018
DESCRIPCI�N:    Tabla encargada de almacenar los movimiento
REALIZ�:        Sapien Gamez Christian Enrique
*****************************************************************************/

CREATE TABLE movimiento(
	clave_movimiento	INT IDENTITY(1,1) NOT NULL PRIMARY KEY,
	clave_empleado		INT NOT NULL DEFAULT -1,
	clave_rol_cubrio	INT NOT NULL DEFAULT -1,
	fecha				DATETIME DEFAULT GETDATE(),
	horas_trabajadas	INT NOT NULL DEFAULT -1,
	entrega				INT NOT NULL DEFAULT -1,
	sueldo_base			DECIMAL NOT NULL DEFAULT -1,
	sueldo_entrega		DECIMAL NOT NULL DEFAULT -1,
	sueldo_bono			DECIMAL NOT NULL DEFAULT -1,
	pagado				INT NOT NULL DEFAULT -1
)
GO