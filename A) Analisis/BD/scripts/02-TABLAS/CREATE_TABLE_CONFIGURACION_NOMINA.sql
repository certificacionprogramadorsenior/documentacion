USE nomina_rinku
GO

--IF EXISTS ( SELECT NAME FROM SysObjects(NOLOCK) WHERE NAME = 'configuracion_nomina' ) 
--    DROP TABLE configuracion_nomina
--GO

/*****************************************************************************
BASE DATOS;     nomina_rinku
TABLA:          configuracion_nomina
FECHA:          14/11/2018
DESCRIPCI�N:    Tabla encargada de almacenar la configuracion de la nomina
REALIZ�:        Sapien Gamez Christian Enrique
*****************************************************************************/

CREATE TABLE configuracion_nomina(
	clave_conf	INT IDENTITY(1,1) NOT NULL PRIMARY KEY,
	nombre		VARCHAR(250) NOT NULL DEFAULT '',
	valor		DECIMAL NOT NULL DEFAULT -1
)
GO