USE nomina_rinku
GO

--IF EXISTS ( SELECT NAME FROM SysObjects(NOLOCK) WHERE NAME = 'rol' ) 
--    DROP TABLE rol
--GO

/*****************************************************************************
BASE DATOS;     nomina_rinku
TABLA:          rol
FECHA:          14/11/2018
DESCRIPCI�N:    Tabla encargada de almacenar los roles
REALIZ�:        Sapien Gamez Christian Enrique
*****************************************************************************/

CREATE TABLE rol(
	clave_rol	INT IDENTITY(1,1) NOT NULL PRIMARY KEY,
	nombre		VARCHAR(250) NOT NULL DEFAULT '',
	bono		DECIMAL NOT NULL DEFAULT -1
)
GO