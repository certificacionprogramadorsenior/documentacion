IF NOT EXISTS(SELECT * FROM nomina_rinku.dbo.rol WHERE clave_rol = 1)
BEGIN
	INSERT INTO nomina_rinku.dbo.rol(nombre, bono) VALUES('CHOFER', 10);
END
ELSE
BEGIN
	UPDATE nomina_rinku.dbo.rol SET nombre = 'CHOFER', bono=10 WHERE clave_rol=1;
END

IF NOT EXISTS(SELECT * FROM nomina_rinku.dbo.rol WHERE clave_rol = 2)
BEGIN
	INSERT INTO nomina_rinku.dbo.rol(nombre, bono) VALUES('CARGADOR', 5);
END
ELSE
BEGIN
	UPDATE nomina_rinku.dbo.rol SET nombre = 'CARGADOR', bono=5 WHERE clave_rol=2;
END

IF NOT EXISTS(SELECT * FROM nomina_rinku.dbo.rol WHERE clave_rol = 3)
BEGIN
	INSERT INTO nomina_rinku.dbo.rol(nombre, bono) VALUES('AUXILIAR', 0);
END
ELSE
BEGIN
	UPDATE nomina_rinku.dbo.rol SET nombre = 'AUXILIAR', bono=0 WHERE clave_rol=3;
END