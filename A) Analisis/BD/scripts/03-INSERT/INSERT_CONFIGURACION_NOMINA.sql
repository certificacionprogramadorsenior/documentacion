IF NOT EXISTS(SELECT * FROM nomina_rinku.dbo.configuracion_nomina WHERE clave_conf = 1)
BEGIN
	INSERT INTO nomina_rinku.dbo.configuracion_nomina(nombre, valor) VALUES('SUELDO BASE', 30);
END
ELSE
BEGIN
	UPDATE nomina_rinku.dbo.configuracion_nomina SET nombre = 'SUELDO BASE', valor=30 WHERE clave_conf=1;
END

IF NOT EXISTS(SELECT * FROM nomina_rinku.dbo.configuracion_nomina WHERE clave_conf = 2)
BEGIN
	INSERT INTO nomina_rinku.dbo.configuracion_nomina(nombre, valor) VALUES('ISR', 9);
END
ELSE
BEGIN
	UPDATE nomina_rinku.dbo.configuracion_nomina SET nombre = 'ISR', valor=9 WHERE clave_conf=2;
END

IF NOT EXISTS(SELECT * FROM nomina_rinku.dbo.configuracion_nomina WHERE clave_conf = 3)
BEGIN
	INSERT INTO nomina_rinku.dbo.configuracion_nomina(nombre, valor) VALUES('ISR ADICIONAL', 3);
END
ELSE
BEGIN
	UPDATE nomina_rinku.dbo.configuracion_nomina SET nombre = 'ISR ADICIONAL', valor=3 WHERE clave_conf=3;
END

IF NOT EXISTS(SELECT * FROM nomina_rinku.dbo.configuracion_nomina WHERE clave_conf = 4)
BEGIN
	INSERT INTO nomina_rinku.dbo.configuracion_nomina(nombre, valor) VALUES('VALES DESPENSA', 4);
END
ELSE
BEGIN
	UPDATE nomina_rinku.dbo.configuracion_nomina SET nombre = 'VALES DESPENSA', valor=4 WHERE clave_conf=4;
END

IF NOT EXISTS(SELECT * FROM nomina_rinku.dbo.configuracion_nomina WHERE clave_conf = 5)
BEGIN
	INSERT INTO nomina_rinku.dbo.configuracion_nomina(nombre, valor) VALUES('ENTREGA AL CLIENTE', 5);
END
ELSE
BEGIN
	UPDATE nomina_rinku.dbo.configuracion_nomina SET nombre = 'ENTREGA AL CLIENTE', valor=5 WHERE clave_conf=5;
END