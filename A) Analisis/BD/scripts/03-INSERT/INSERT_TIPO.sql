IF NOT EXISTS(SELECT * FROM nomina_rinku.dbo.tipo WHERE clave_tipo = 1)
BEGIN
	INSERT INTO nomina_rinku.dbo.tipo(nombre) VALUES('INTERNO');
END
ELSE
BEGIN
	UPDATE nomina_rinku.dbo.tipo SET nombre = 'INTERNO' WHERE clave_tipo=1;
END

IF NOT EXISTS(SELECT * FROM nomina_rinku.dbo.tipo WHERE clave_tipo = 2)
BEGIN
	INSERT INTO nomina_rinku.dbo.tipo(nombre) VALUES('EXTERNO');
END
ELSE
BEGIN
	UPDATE nomina_rinku.dbo.tipo SET nombre = 'EXTERNO' WHERE clave_tipo=2;
END